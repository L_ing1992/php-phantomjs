<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs;

use Ling\PhantomJs\Http\AbstractRequest;
use Ling\PhantomJs\Http\MessageFactoryInterface;
use Ling\PhantomJs\Http\RequestInterface;
use Ling\PhantomJs\Http\ResponseInterface;
use Ling\PhantomJs\Procedure\ProcedureLoaderInterface;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@Ling.me>
 */
interface ClientInterface
{
    /**
     * Get singleton instance
     *
     * @access public
     * @return ClientInterface
     */
    public static function getInstance(): ClientInterface;

    /**
     * Get engine instance.
     *
     * @access public
     * @return Engine
     */
    public function getEngine(): Engine;

    /**
     * Get message factory instance
     *
     * @access public
     * @return MessageFactoryInterface
     */
    public function getMessageFactory(): Http\MessageFactoryInterface;

    /**
     * Get procedure loader instance
     *
     * @access public
     * @return ProcedureLoaderInterface
     */
    public function getProcedureLoader(): Procedure\ProcedureLoaderInterface;

    /**
     * Send request
     *
     * @access public
     * @param AbstractRequest $request
     * @param ResponseInterface $response
     */
    public function send(AbstractRequest $request, ResponseInterface $response);

    /**
     * Get log.
     *
     * @access public
     * @return string
     */
    public function getLog(): string;

    /**
     * Set procedure template.
     *
     * @access public
     * @param string $procedure
     * @return ClientInterface
     */
    public function setProcedure(string $procedure): ClientInterface;

    /**
     * Get procedure template.
     *
     * @access public
     * @return string
     */
    public function getProcedure(): string;

    /**
     * Set lazy request flag.
     *
     * @access public
     * @return ClientInterface
     */
    public function isLazy(): ClientInterface;
}

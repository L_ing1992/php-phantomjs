<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Cache;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface CacheInterface
{
    /**
     * Write data to storage.
     *
     * @access public
     * @param string $id
     * @param string $data
     * @return string
     */
    public function save(string $id, string $data): string;

    /**
     * Fetch data from file.
     *
     * @access public
     * @param string $id
     * @return string|false
     */
    public function fetch(string $id): string|false;

    /**
     * Delete data from storage.
     *
     * @access public
     * @param string $id
     * @return void
     */
    public function delete(string $id): void;

    /**
     * Data exists in storage.
     *
     * @access public
     * @param string $id
     * @return boolean
     */
    public function exists(string $id): bool;
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class Request extends AbstractRequest
{
    /**
     * Request type
     *
     * @var string
     * @access protected
     */
    protected string $type;

    /**
     * Get request type
     *
     * @access public
     * @return string
     */
    public function getType(): string
    {
        if (!isset($this->type) || !$this->type) {
            return RequestInterface::REQUEST_TYPE_DEFAULT;
        }

        return $this->type;
    }

    /**
     * Set request type
     *
     * @access public
     * @param string $type
     * @return Request
     */
    public function setType(string $type): Request
    {
        $this->type = $type;
        return $this;
    }
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@Ling.me>
 */
interface MessageFactoryInterface
{
    /**
     * Get singleton instance.
     *
     * @access public
     * @return MessageFactoryInterface
     */
    public static function getInstance(): MessageFactoryInterface;

    /**
     * Create request instance.
     *
     * @access public
     * @param string|null $url (default: null)
     * @param string $method (default: RequestInterface::METHOD_GET)
     * @param int $timeout (default: 5000)
     * @return RequestInterface
     */
    public function createRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): RequestInterface;

    /**
     * Create capture request instance.
     *
     * @access public
     * @param string|null $url (default: null)
     * @param string $method (default: RequestInterface::METHOD_GET)
     * @param int $timeout (default: 5000)
     * @return RequestInterface
     */
    public function createCaptureRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): RequestInterface;

    /**
     * Create PDF request instance.
     *
     * @access public
     * @param string|null $url
     * @param string $method
     * @param int $timeout
     * @return RequestInterface
     */
    public function createPdfRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): RequestInterface;

    /**
     * Create response instance.
     *
     * @access public
     * @return ResponseInterface
     */
    public function createResponse(): ResponseInterface;
}

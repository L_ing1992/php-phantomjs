<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs.
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface CaptureRequestInterface
{
    /**
     * Set viewport size.
     *
     * @param int $width
     * @param int $height
     * @param int $top    (default: 0)
     * @param int $left   (default: 0)
     */
    public function setCaptureDimensions(int $width, int $height, int $top = 0, int $left = 0): CaptureRequestInterface;

    /**
     * Get rect top.
     *
     * @return int
     */
    public function getRectTop(): int;

    /**
     * Get rect left.
     *
     * @return int
     */
    public function getRectLeft(): int;

    /**
     * Get rect width.
     *
     * @return int
     */
    public function getRectWidth(): int;

    /**
     * Get rect height.
     *
     * @return int
     */
    public function getRectHeight(): int;

    /**
     * Set file to save output.
     *
     * @param string $file
     */
    public function setOutputFile(string $file): CaptureRequestInterface;

    /**
     * Get output file.
     *
     * @return string
     */
    public function getOutputFile(): string;

    /**
     * Get image format of the capture.
     *
     * @return string
     */
    public function getFormat(): string;

    /**
     * Set image format of capture.
     * options: pdf, png, jpeg, bmp, ppm, gif.
     *
     * @param string $format
     */
    public function setFormat(string $format): CaptureRequestInterface;

    /**
     * Get quality of capture.
     *
     * @return int
     */
    public function getQuality(): int;

    /**
     * Set quality of the capture.
     * example: 0 - 100.
     *
     * @param int $quality
     */
    public function setQuality(int $quality): CaptureRequestInterface;
}

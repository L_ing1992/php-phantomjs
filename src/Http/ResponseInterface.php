<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface ResponseInterface
{
    /**
     * Import response data
     *
     * @access public
     */
    public function import(array $data): ResponseInterface;

    /**
     * Get HTTP headers array
     *
     * @access public
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Get HTTP header value for code
     *
     * @access public
     * @param string $code
     * @return mixed
     */
    public function getHeader(string $code): mixed;

    /**
     * Get response status code
     *
     * @access public
     * @return integer
     */
    public function getStatus(): int;

    /**
     * Get page content from respone
     *
     * @access public
     * @return string
     */
    public function getContent(): string;

    /**
     * Get content type header
     *
     * @access public
     * @return string
     */
    public function getContentType(): string;

    /**
     * Get request URL
     *
     * @access public
     * @return string
     */
    public function getUrl(): string;

    /**
     * Get redirect URL (if redirected)
     *
     * @access public
     * @return string
     */
    public function getRedirectUrl(): string;

    /**
     * Is response a redirect
     *  - Checks status codes
     *
     * @access public
     * @return boolean
     */
    public function isRedirect(): bool;

    /**
     * Get time string
     *
     * @access public
     * @return string
     */
    public function getTime(): string;

    /**
     * Get session cookies
     *
     * @access public
     * @return array
     */
    public function getCookies(): array;
}

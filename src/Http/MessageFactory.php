<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

use Ling\PhantomJs\Exception\InvalidMethodException;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@Ling.me>
 */
class MessageFactory implements MessageFactoryInterface
{
    /**
     * Client instance
     *
     * @var MessageFactory
     * @access private
     */
    private static MessageFactory $instance;

    /**
     * Get singleton instance.
     *
     * @access public
     * @return MessageFactory
     */
    public static function getInstance(): MessageFactory
    {
        if (!self::$instance instanceof MessageFactoryInterface) {
            self::$instance = new MessageFactory();
        }

        return self::$instance;
    }

    /**
     * Create request instance.
     *
     * @access public
     * @param string|null $url
     * @param string $method
     * @param int $timeout
     * @return Request
     * @throws InvalidMethodException
     */
    public function createRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): Request
    {
        return new Request($url, $method, $timeout);
    }

    /**
     * Create capture request instance.
     *
     * @access public
     * @param string|null $url
     * @param string $method
     * @param int $timeout
     * @return CaptureRequest
     * @throws InvalidMethodException
     */
    public function createCaptureRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): RequestInterface
    {
        return new CaptureRequest($url, $method, $timeout);
    }

    /**
     * Create PDF request instance.
     *
     * @access public
     * @param string|null $url
     * @param string $method
     * @param int $timeout
     * @return PdfRequest
     * @throws InvalidMethodException
     */
    public function createPdfRequest(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000): PdfRequest
    {
        return new PdfRequest($url, $method, $timeout);
    }

    /**
     * Create response instance.
     *
     * @access public
     * @return Response
     */
    public function createResponse(): ResponseInterface
    {
        return new Response();
    }
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface RequestInterface
{
    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_GET = 'GET';
    const METHOD_HEAD = 'HEAD';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PATCH = 'PATCH';

    const REQUEST_TYPE_DEFAULT = 'default';
    const REQUEST_TYPE_CAPTURE = 'capture';
    const REQUEST_TYPE_PDF = 'pdf';

    /**
     * Get request type
     *
     * @access public
     * @return string
     */
    public function getType(): string;

    /**
     * Set request method
     *
     * @access public
     * @param string $method
     */
    public function setMethod(string $method): RequestInterface;

    /**
     * Get request method
     *
     * @access public
     * @return string
     */
    public function getMethod(): string;

    /**
     * Set timeout period
     *
     * @access public
     * @param int $timeout
     */
    public function setTimeout(int $timeout): RequestInterface;

    /**
     * Get timeout period
     *
     * @access public
     * @return int
     */
    public function getTimeout(): int;

    /**
     * Set page load delay time.
     *
     * @access public
     * @param int $delay
     */
    public function setDelay(int $delay): RequestInterface;

    /**
     * Get page load delay time.
     *
     * @access public
     * @return int
     */
    public function getDelay(): int;

    /**
     * Set viewport size.
     *
     * @access public
     * @param int $width
     * @param int $height
     */
    public function setViewportSize(int $width, int $height): RequestInterface;

    /**
     * Get viewport width.
     *
     * @access public
     * @return int
     */
    public function getViewportWidth(): int;

    /**
     * Get viewport height.
     *
     * @access public
     * @return int
     */
    public function getViewportHeight(): int;

    /**
     * Set request URL
     *
     * @access public
     * @param string $url
     */
    public function setUrl(string $url): RequestInterface;

    /**
     * Get request URL
     *
     * @access public
     * @return string
     */
    public function getUrl(): string;

    /**
     * Get content body
     *
     * @access public
     * @return string
     */
    public function getBody(): string;

    /**
     * Set request data
     *
     * @access public
     * @param array $data
     */
    public function setRequestData(array $data): RequestInterface;

    /**
     * Get request data
     *
     * @access public
     * @param boolean $flat
     * @return array
     */
    public function getRequestData(bool $flat = true): array;

    /**
     * Set headers
     *
     * @access public
     * @param array $headers
     */
    public function setHeaders(array $headers): RequestInterface;

    /**
     * Add single header
     *
     * @access public
     * @param string $header
     * @param string $value
     */
    public function addHeader(string $header, string $value): RequestInterface;

    /**
     * Merge headers with existing
     *
     * @access public
     * @param array $headers
     */
    public function addHeaders(array $headers): RequestInterface;

    /**
     * Get request headers
     *
     * @access public
     * @return array|string
     */
    public function getHeaders(): array|string;

    /**
     * Get settings
     *
     * @access public
     * @return array
     */
    public function getSettings(): array;

    /**
     * Get cookies
     *
     * @access public
     * @return array
     */
    public function getCookies(): array;

    /**
     * Set body styles
     *
     * @access public
     * @param array $styles
     */
    public function setBodyStyles(array $styles): RequestInterface;

    /**
     * Get body styles
     *
     * @access public
     * @return array|string
     */
    public function getBodyStyles(): array|string;
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface PdfRequestInterface
{
    /**
     * Set paper width.
     *
     * @access public
     * @param string $width
     * @return PdfRequestInterface
     */
    public function setPaperWidth(string $width) : PdfRequestInterface;

    /**
     * Get paper width.
     *
     * @access public
     * @return string
     */
    public function getPaperWidth(): string;

    /**
     * Set paper height.
     *
     * @access public
     * @param string $height
     * @return PdfRequestInterface
     */
    public function setPaperHeight(string $height): PdfRequestInterface;

    /**
     * Get paper height.
     *
     * @access public
     * @return string
     */
    public function getPaperHeight(): string;

    /**
     * Set paper size.
     *
     * @access public
     * @param string $width
     * @param string $height
     * @return PdfRequestInterface
     */
    public function setPaperSize(string $width, string $height): PdfRequestInterface;

    /**
     * Set format.
     *
     * @access public
     * @param string $format
     * @return PdfRequestInterface
     */
    public function setFormat(string $format): PdfRequestInterface;

    /**
     * Get format.
     *
     * @access public
     * @return string
     */
    public function getFormat(): string;

    /**
     * Set orientation.
     *
     * @access public
     * @param string $orientation
     * @return PdfRequestInterface
     */
    public function setOrientation(string $orientation): PdfRequestInterface;

    /**
     * Get orientation.
     *
     * @access public
     * @return string
     */
    public function getOrientation(): string;

    /**
     * Set margin.
     *
     * @access public
     * @param string $margin
     * @return PdfRequestInterface
     */
    public function setMargin(string $margin): PdfRequestInterface;

    /**
     * Get margin.
     *
     * @access public
     * @return string
     */
    public function getMargin(): string;

    /**
     * Set repeating header.
     *
     * @access public
     * @param string $content
     * @param string $height  (default: '1cm')
     * @return PdfRequestInterface
     */
    public function setRepeatingHeader(string $content, string $height = '1cm'): PdfRequestInterface;

    /**
     * Get repeating header.
     *
     * @access public
     * @return array
     */
    public function getRepeatingHeader(): array;

    /**
     * Set repeating footer.
     *
     * @access public
     * @param string $content
     * @param string $height  (default: '1cm')
     * @return PdfRequestInterface
     */
    public function setRepeatingFooter(string $content, string $height = '1cm'): PdfRequestInterface;

    /**
     * Get repeating footer.
     *
     * @access public
     * @return array
     */
    public function getRepeatingFooter(): array;
}

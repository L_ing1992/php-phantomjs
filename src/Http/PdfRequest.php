<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Http;

use Ling\PhantomJs\Exception\InvalidMethodException;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class PdfRequest extends CaptureRequest
    implements PdfRequestInterface
{
    /**
     * Paper width
     *
     * @var string
     * @access protected
     */
    protected string $paperWidth;

    /**
     * Paper height
     *
     * @var string
     * @access protected
     */
    protected string $paperHeight;

    /**
     * Format
     *
     * @var string
     * @access protected
     */
    protected string $format;

    /**
     * Orientation
     *
     * @var string
     * @access protected
     */
    protected string $orientation;

    /**
     * Margin
     *
     * @var string
     * @access protected
     */
    protected string $margin;

    /**
     * Repeating header
     *
     * @var array
     * @access protected
     */
    protected array $header;

    /**
     * Repeating footer
     *
     * @var array
     * @access protected
     */
    protected array $footer;

    /**
     * Internal constructor
     *
     * @access public
     * @param string|null $url (default: null)
     * @param string $method (default: RequestInterface::METHOD_GET)
     * @param int $timeout (default: 5000)
     * @throws InvalidMethodException
     */
    public function __construct(string $url = null, string $method = RequestInterface::METHOD_GET, int $timeout = 5000)
    {
        parent::__construct($url, $method, $timeout);

        $this->paperWidth = '';
        $this->paperHeight = '';
        $this->margin = '1cm';
        $this->format = 'A4';
        $this->orientation = 'portrait';
        $this->header = array();
        $this->footer = array();

    }

    /**
     * Get request type
     *
     * @access public
     * @return string
     */
    public function getType(): string
    {
        if (!$this->type) {
            return RequestInterface::REQUEST_TYPE_PDF;
        }

        return $this->type;
    }

    /**
     * Set paper width.
     *
     * @access public
     * @param string $width
     * @return PdfRequest
     */
    public function setPaperWidth(string $width): PdfRequest
    {
        $this->paperWidth = $width;
        return $this;
    }

    /**
     * Get paper width.
     *
     * @access public
     * @return string
     */
    public function getPaperWidth(): string
    {
        return $this->paperWidth;
    }

    /**
     * Set paper height.
     *
     * @access public
     * @param string $height
     * @return PdfRequest
     */
    public function setPaperHeight(string $height): PdfRequest
    {
        $this->paperHeight = $height;
        return $this;
    }

    /**
     * Get paper height.
     *
     * @access public
     * @return string
     */
    public function getPaperHeight(): string
    {
        return $this->paperHeight;
    }

    /**
     * Set paper size.
     *
     * @access public
     * @param string $width
     * @param string $height
     * @return PdfRequest
     */
    public function setPaperSize(string $width, string $height): PdfRequest
    {
        $this->paperWidth = $width;
        $this->paperHeight = $height;
        return $this;
    }

    /**
     * Set format.
     *
     * @access public
     * @param string $format
     * @return PdfRequest
     */
    public function setFormat(string $format): PdfRequest
    {
        $this->format = $format;
        return $this;
    }

    /**
     * Get format.
     *
     * @access public
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * Set orientation.
     *
     * @access public
     * @param string $orientation
     * @return void
     */
    public function setOrientation(string $orientation): PdfRequest
    {
        $this->orientation = $orientation;
        return $this;
    }

    /**
     * Get orientation.
     *
     * @access public
     * @return string
     */
    public function getOrientation(): string
    {
        return $this->orientation;
    }

    /**
     * Set margin.
     *
     * @access public
     * @param string $margin
     * @return PdfRequest
     */
    public function setMargin(string $margin): PdfRequest
    {
        $this->margin = $margin;
        return $this;
    }

    /**
     * Get margin.
     *
     * @access public
     * @return string
     */
    public function getMargin(): string
    {
        return $this->margin;
    }

    /**
     * Set repeating header.
     *
     * @access public
     * @param string $content
     * @param string $height (default: '1cm')
     * @return PdfRequest
     */
    public function setRepeatingHeader(string $content, string $height = '1cm'): PdfRequest
    {
        $this->header = [
            'content' => $content,
            'height' => $height
        ];
        return $this;
    }

    /**
     * Get repeating header.
     *
     * @access public
     * @return array
     */
    public function getRepeatingHeader(): array
    {
        return $this->header;
    }

    /**
     * Set repeating footer.
     *
     * @access public
     * @param string $content
     * @param string $height (default: '1cm')
     * @return PdfRequest
     */
    public function setRepeatingFooter(string $content, string $height = '1cm'): PdfRequest
    {
        $this->footer = [
            'content' => $content,
            'height' => $height
        ];
        return $this;
    }

    /**
     * Get repeating footer.
     *
     * @access public
     * @return array
     */
    public function getRepeatingFooter(): array
    {
        return $this->footer;
    }
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Parser;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class JsonParser implements ParserInterface
{
    /**
     * Parse json string into array.
     *
     * @access public
     * @param mixed $data
     * @return array
     */
    public function parse(mixed $data): array
    {
        if (!is_string($data)) {
            return array();
        }
        if (!str_starts_with($data, '{') &&
            !str_starts_with($data, '[')) {
            return array();
        }
        return json_decode($data, true);
    }
}

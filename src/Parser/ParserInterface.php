<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Parser;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface ParserInterface
{
    /**
     * Parse data.
     *
     * @access public
     * @param mixed $data
     */
    public function parse(mixed $data): array;
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Validator\EngineInterface;
use Ling\PhantomJs\Exception\SyntaxException;
use Ling\PhantomJs\Exception\RequirementException;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class ProcedureValidator implements ProcedureValidatorInterface
{
    /**
     * Procedure loader.
     *
     * @var ProcedureLoaderInterface
     * @access protected
     */
    protected ProcedureLoaderInterface $procedureLoader;

    /**
     * Validator engine
     *
     * @var EngineInterface
     * @access protected
     */
    protected EngineInterface $engine;

    /**
     * Internal constructor.
     *
     * @access public
     * @param ProcedureLoaderInterface $procedureLoader
     * @param EngineInterface $engine
     */
    public function __construct(ProcedureLoaderInterface $procedureLoader, EngineInterface $engine)
    {
        $this->procedureLoader = $procedureLoader;
        $this->engine = $engine;
    }

    /**
     * Validate procedure.
     *
     * @access public
     * @param string $procedure
     * @return boolean
     * @throws RequirementException
     * @throws SyntaxException
     */
    public function validate(string $procedure): bool
    {
        $this->validateSyntax($procedure);
        $this->validateRequirements($procedure);

        return true;
    }

    /**
     * Validate syntax.
     *
     * @access protected
     * @param string $procedure
     * @return void
     * @throws SyntaxException
     */
    protected function validateSyntax(string $procedure): void
    {
        $input = new Input();
        $output = new Output();

        $input->set('procedure', $procedure);
        $input->set('engine', $this->engine->toString());

        $validator = $this->procedureLoader->load('validator');
        $validator->run($input, $output);

        $errors = $output->get('errors');

        if (!empty($errors)) {
            throw new SyntaxException('Your procedure failed to compile due to a javascript syntax error', (array)$errors);
        }
    }

    /**
     * validateRequirements function.
     *
     * @access protected
     * @param string $procedure
     * @return void
     * @throws RequirementException
     */
    protected function validateRequirements(string $procedure): void
    {
        if (preg_match('/phantom\.exit\(/', $procedure, $matches) !== 1) {
            throw new RequirementException('Your procedure must contain a \'phantom.exit(1);\' command to avoid the PhantomJS process hanging');
        }
    }
}

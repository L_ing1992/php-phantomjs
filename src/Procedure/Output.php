<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class Output implements OutputInterface
{
    /**
     * Output data.
     *
     * @var array
     * @access protected
     */
    protected array $data;

    /**
     * Output logs.
     *
     * @var array
     * @access protected
     */
    protected array $logs;

    /**
     * Internal constructor.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->data = array();
        $this->logs = array();
    }

    /**
     * Import data.
     *
     * @param array $data
     * @return Output
     * @access public
     */
    public function import(array $data): Output
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Set data value.
     *
     * @access public
     * @param string $name
     * @param mixed $value
     * @return Output
     */
    public function set(string $name, mixed $value): Output
    {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * Get data value.
     *
     * @access public
     * @param string $name
     * @return mixed
     */
    public function get(string $name): mixed
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return '';
    }

    /**
     * Log data.
     *
     * @access public
     * @param string $data
     */
    public function log(string $data): void
    {
        $this->logs[] = $data;
    }

    /**
     * Get log data.
     *
     * @access public
     * @return array
     */
    public function getLogs(): array
    {
        return $this->logs;
    }
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Engine;
use Ling\PhantomJs\Cache\CacheInterface;
use Ling\PhantomJs\Parser\ParserInterface;
use Ling\PhantomJs\Template\TemplateRendererInterface;
use Ling\PhantomJs\Exception\NotWritableException;
use Ling\PhantomJs\Exception\ProcedureFailedException;
use Ling\PhantomJs\StringUtils;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class Procedure implements ProcedureInterface
{
    /**
     * PhantomJS engine
     *
     * @var Engine
     * @access protected
     */
    protected Engine $engine;

    /**
     * Parser instance.
     *
     * @var ParserInterface
     * @access protected
     */
    protected ParserInterface $parser;

    /**
     * Cache handler instance.
     *
     * @var CacheInterface
     * @access protected
     */
    protected CacheInterface $cacheHandler;

    /**
     * Template renderer.
     *
     * @var TemplateRendererInterface
     * @access protected
     */
    protected TemplateRendererInterface $renderer;

    /**
     * Procedure template.
     *
     * @var string
     * @access protected
     */
    protected string $template;

    /**
     * Internal constructor.
     *
     * @access public
     * @param Engine $engine
     * @param ParserInterface $parser
     * @param CacheInterface $cacheHandler
     * @param TemplateRendererInterface $renderer
     */
    public function __construct(Engine $engine, ParserInterface $parser, CacheInterface $cacheHandler, TemplateRendererInterface $renderer)
    {
        $this->engine       = $engine;
        $this->parser       = $parser;
        $this->cacheHandler = $cacheHandler;
        $this->renderer     = $renderer;
    }

    /**
     * Run procedure.
     *
     * @access public
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws NotWritableException
     * @throws ProcedureFailedException
     */
    public function run(InputInterface $input, OutputInterface $output): void
    {
        try {
            $executable = $this->write($this->compile($input));

            $descriptorspec = array(
                array('pipe', 'r'),
                array('pipe', 'w'),
                array('pipe', 'w')
            );

            $process = proc_open(escapeshellcmd(sprintf('%s %s', $this->engine->getCommand(), $executable)), $descriptorspec, $pipes, null, null);

            if (!is_resource($process)) {
                throw new ProcedureFailedException('proc_open() did not return a resource');
            }

            $result = stream_get_contents($pipes[1]);
            $log    = stream_get_contents($pipes[2]);

            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);

            proc_close($process);

            $output->import(
                $this->parser->parse($result)
            );

            $this->engine->log($log);

            $this->remove($executable);

        } catch (NotWritableException $e) {
            throw $e;
        } catch (\Exception $e) {

            if (isset($executable)) {
                $this->remove($executable);
            }

            throw new ProcedureFailedException(sprintf('Error when executing PhantomJs procedure - %s', $e->getMessage()));
        }
    }

    /**
     * Set procedure template.
     *
     * @access public
     * @param string $template
     * @return Procedure
     */
    public function setTemplate(string $template): Procedure
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Get procedure template.
     *
     * @access public
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Compile procedure.
     *
     * @access public
     * @param InputInterface $input
     * @return string
     */
    public function compile(InputInterface $input): string
    {
       return $this->renderer->render($this->getTemplate(), array('input' => $input));
    }

    /**
     * Write compiled procedure to cache.
     *
     * @access protected
     * @param string $compiled
     * @return string
     */
    protected function write(string $compiled): string
    {
        return $this->cacheHandler->save(StringUtils::random(20), $compiled);
    }

    /**
     * Remove procedure script cache.
     *
     * @access protected
     * @param string $filePath
     * @return void
     */
    protected function remove(string $filePath): void
    {
        $this->cacheHandler->delete($filePath);
    }
}

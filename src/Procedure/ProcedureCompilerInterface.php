<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Procedure\InputInterface;
use Ling\PhantomJs\Procedure\ProcedureInterface;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface ProcedureCompilerInterface
{
    /**
     * Compile partials into procedure.
     *
     * @access public
     * @param \Ling\PhantomJs\Procedure\ProcedureInterface $procedure
     * @param \Ling\PhantomJs\Procedure\InputInterface $input
     */
    public function compile(ProcedureInterface $procedure, InputInterface $input): void;

    /**
     * Load partial template.
     *
     * @access public
     * @param string $name
     * @return string
     */
    public function load(string $name): string;

    /**
     * Enable cache.
     *
     * @access public
     * @return void
     */
    public function enableCache(): void;

    /**
     * Disable cache.
     *
     * @access public
     * @return void
     */
    public function disableCache(): void;

    /**
     * Clear cache.
     *
     * @access public
     * @return void
     */
    public function clearCache(): void;
}

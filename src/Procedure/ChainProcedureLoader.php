<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Procedure;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class ChainProcedureLoader implements ProcedureLoaderInterface
{
    /**
     * Procedure loader storage.
     *
     * @var array
     * @access protected
     */
    protected array $procedureLoaders;

    /**
     * Internal constructor.
     *
     * @access public
     * @param array $procedureLoaders
     */
    public function __construct(array $procedureLoaders)
    {
        $this->procedureLoaders = $procedureLoaders;
    }

    /**
     * Add procedure loader.
     *
     * @access public
     * @param ProcedureLoaderInterface $procedureLoader
     * @return void
     */
    public function addLoader(ProcedureLoaderInterface $procedureLoader): void
    {
        array_unshift($this->procedureLoaders, $procedureLoader);
    }

    /**
     * Load procedure instance by id.
     *
     * @access public
     * @param string $id
     * @return ProcedureInterface
     * @throws \InvalidArgumentException
     */
    public function load(string $id): ProcedureInterface
    {
        /** @var ProcedureLoaderInterface $loader **/
        foreach ($this->procedureLoaders as $loader) {
            try {
                return $loader->load($id);
            } catch (\Exception $e) {}
        }
        throw new \InvalidArgumentException(sprintf('No valid procedure loader could be found to load the \'%s\' procedure.', $id));
    }

    /**
     * Load procedure template by id.
     *
     * @access public
     * @param string $id
     * @param string $extension (default: 'proc')
     * @return string
     *@throws \InvalidArgumentException
     */
    public function loadTemplate(string $id, string $extension = 'proc'): string
    {
        /** @var ProcedureLoaderInterface $loader **/
        foreach ($this->procedureLoaders as $loader) {
            try {
                return $loader->loadTemplate($id, $extension);
            } catch (\Exception $e) {}
        }
        throw new \InvalidArgumentException(sprintf('No valid procedure loader could be found to load the \'%s\' procedure template.', $id));
    }
}

<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Engine;
use Ling\PhantomJs\Cache\CacheInterface;
use Ling\PhantomJs\Parser\ParserInterface;
use Ling\PhantomJs\Template\TemplateRendererInterface;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class ProcedureFactory implements ProcedureFactoryInterface
{
    /**
     * PhantomJS engine
     *
     * @var Engine
     * @access protected
     */
    protected Engine $engine;

    /**
     * Parser.
     *
     * @var ParserInterface
     * @access protected
     */
    protected ParserInterface $parser;

    /**
     * Cache handler.
     *
     * @var CacheInterface
     * @access protected
     */
    protected CacheInterface $cacheHandler;

    /**
     * Template renderer.
     *
     * @var TemplateRendererInterface
     * @access protected
     */
    protected TemplateRendererInterface $renderer;

    /**
     * Internal constructor.
     *
     * @access public
     * @param Engine $engine
     * @param ParserInterface $parser
     * @param CacheInterface $cacheHandler
     * @param TemplateRendererInterface $renderer
     */
    public function __construct(Engine $engine, ParserInterface $parser, CacheInterface $cacheHandler, TemplateRendererInterface $renderer)
    {
        $this->engine       = $engine;
        $this->parser       = $parser;
        $this->cacheHandler = $cacheHandler;
        $this->renderer     = $renderer;
    }

    /**
     * Create new procedure instance.
     *
     * @access public
     * @return Procedure
     */
    public function createProcedure(): Procedure
    {
        return  new Procedure(
            $this->engine,
            $this->parser,
            $this->cacheHandler,
            $this->renderer
        );
    }
}

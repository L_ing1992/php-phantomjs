<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ling\PhantomJs\Procedure;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface ProcedureLoaderInterface
{
    /**
     * Load procedure instance by id.
     *
     * @access public
     * @param string $id
     * @return ProcedureInterface
     */
    public function load(string $id): ProcedureInterface;

    /**
     * Load procedure template by id.
     *
     * @access public
     * @param string $id
     * @param string $extension (default: 'proc')
     * @return string
     */
    public function loadTemplate(string $id, string $extension = 'proc'): string;
}

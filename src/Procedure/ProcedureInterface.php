<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Procedure\InputInterface;
use Ling\PhantomJs\Procedure\OutputInterface;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
interface ProcedureInterface
{
    /**
     * Run procedure.
     *
     * @access public
     * @param \Ling\PhantomJs\Procedure\InputInterface $input
     * @param \Ling\PhantomJs\Procedure\OutputInterface $output
     */
    public function run(InputInterface $input, OutputInterface $output): void;

    /**
     * Set procedure template.
     *
     * @access public
     * @param string $template
     */
    public function setTemplate(string $template): ProcedureInterface;

    /**
     * Get procedure template.
     *
     * @access public
     * @return string
     */
    public function getTemplate(): string;

    /**
     * Compile procedure.
     *
     * @access public
     * @param \Ling\PhantomJs\Procedure\InputInterface $input
     * @return string
     */
    public function compile(InputInterface $input): string;
}

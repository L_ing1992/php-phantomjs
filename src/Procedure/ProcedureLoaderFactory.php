<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Symfony\Component\Config\FileLocator;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class ProcedureLoaderFactory implements ProcedureLoaderFactoryInterface
{
    /**
     * Procedure factory.
     *
     * @var ProcedureFactoryInterface
     * @access protected
     */
    protected ProcedureFactoryInterface $procedureFactory;

    /**
     * Internal constructor.
     *
     * @access public
     * @param ProcedureFactoryInterface $procedureFactory
     */
    public function __construct(ProcedureFactoryInterface $procedureFactory)
    {
        $this->procedureFactory = $procedureFactory;
    }

    /**
     * Create procedure loader instance.
     *
     * @access public
     * @param string $directory
     * @return ProcedureLoader
     */
    public function createProcedureLoader(string $directory): ProcedureLoader
    {
        $procedureFactory = $this->procedureFactory;
        $fileLocator      = $this->createFileLocator($directory);

        return  new ProcedureLoader(
            $procedureFactory,
            $fileLocator
        );
    }

    /**
     * Create file locator instance.
     *
     * @access protected
     * @param string $directory
     * @return FileLocator
     * @throws \InvalidArgumentException
     */
    protected function createFileLocator(string $directory): FileLocator
    {
        if (!is_dir($directory) || !is_readable($directory)) {
            throw new \InvalidArgumentException(sprintf('Could not create procedure loader as directory does not exist or is not readable: "%s"', $directory));
        }

        return new FileLocator($directory);
    }
}

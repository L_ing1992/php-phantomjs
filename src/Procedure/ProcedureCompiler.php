<?php

/*
 * This file is part of the php-phantomjs.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ling\PhantomJs\Procedure;

use Ling\PhantomJs\Cache\CacheInterface;
use Ling\PhantomJs\Template\TemplateRendererInterface;

/**
 * PHP PhantomJs
 *
 * @author Jon Wenmoth <contact@jonnyw.me>
 */
class ProcedureCompiler implements ProcedureCompilerInterface
{
    /**
     * Procedure loader
     *
     * @var ProcedureLoaderInterface
     * @access protected
     */
    protected ProcedureLoaderInterface $procedureLoader;

    /**
     * Procedure validator
     *
     * @var ProcedureValidatorInterface
     * @access protected
     */
    protected ProcedureValidatorInterface $procedureValidator;

    /**
     * Cache handler
     *
     * @var CacheInterface
     * @access protected
     */
    protected CacheInterface $cacheHandler;

    /**
     * Renderer
     *
     * @var TemplateRendererInterface
     * @access protected
     */
    protected TemplateRendererInterface $renderer;

    /**
     * Cache enabled
     *
     * @var boolean
     * @access protected
     */
    protected bool $cacheEnabled;

    /**
     * Internal constructor
     *
     * @access public
     * @param ProcedureLoaderInterface $procedureLoader
     * @param ProcedureValidatorInterface $procedureValidator
     * @param CacheInterface $cacheHandler
     * @param TemplateRendererInterface $renderer
     */
    public function __construct(ProcedureLoaderInterface $procedureLoader, ProcedureValidatorInterface $procedureValidator,
        CacheInterface $cacheHandler, TemplateRendererInterface $renderer)
    {
        $this->procedureLoader    = $procedureLoader;
        $this->procedureValidator = $procedureValidator;
        $this->cacheHandler       = $cacheHandler;
        $this->renderer           = $renderer;
        $this->cacheEnabled       = true;
    }

    /**
     * Compile partials into procedure.
     *
     * @access public
     * @param ProcedureInterface $procedure
     * @param InputInterface $input
     * @return void
     */
    public function compile(ProcedureInterface $procedure, InputInterface $input): void
    {
        $cacheKey = sprintf('phantomjs_%s_%s', $input->getType(), md5($procedure->getTemplate()));

        if ($this->cacheEnabled && $this->cacheHandler->exists($cacheKey)) {
            $template = $this->cacheHandler->fetch($cacheKey);
        }

        if (empty($template)) {

            $template  = $this->renderer
                ->render($procedure->getTemplate(), array('engine' => $this, 'procedure_type' => $input->getType()));

            $test = clone $procedure;
            $test->setTemplate($template);

            $compiled = $test->compile($input);

            $this->procedureValidator->validate($compiled);

            if ($this->cacheEnabled) {
                $this->cacheHandler->save($cacheKey, $template);
            }
        }

        $procedure->setTemplate($template);
    }

    /**
     * Load partial template.
     *
     * @access public
     * @param string $name
     * @return string
     */
    public function load(string $name): string
    {
        return $this->procedureLoader->loadTemplate($name, 'partial');
    }

    /**
     * Enable cache.
     *
     * @access public
     * @return void
     */
    public function enableCache(): void
    {
        $this->cacheEnabled = true;
    }

    /**
     * Disable cache.
     *
     * @access public
     * @return void
     */
    public function disableCache(): void
    {
        $this->cacheEnabled = false;
    }

    /**
     * Clear cache.
     *
     * @access public
     * @return void
     */
    public function clearCache(): void
    {
        $this->cacheHandler->delete('phantomjs_*');
    }
}
